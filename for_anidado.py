"""
Haciendo uso de un ciclo anidado FOR simule que: en cada una de las entidades, cada
persona realiza un pago de cada servicio.
"""
Entidad=["BN popular","BN Nacional","BN Central de Costa Rica"]

Personal=["Juan","Alberto","Yessenia"]

Servicio=["Servicios Municipales","Pago Luz","Pago Agua"]


for i in Entidad:
    for x in Personal:
        for j in Servicio:
            print("Deposito en ",i," de",x," por Consepto de pago de : ",j)