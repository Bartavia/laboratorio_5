"""
El siguiente ejemplo de código contiene errores corríjalos para que el ejemplo se ejecute
de manera correcta, indique en comentarios cuales fueron los errores encontrados
"""
class Articulo:
    def __init__(self,codigo,nombre):
        self.codigo=codigo
        self.nombre=nombre

    def getNombre(self,):
        return "Lapiz"

    def getColor(self):
        return "rojo"

class Juguete(Articulo):
    def __init__(self, codigo, nombre, precio):
        super().__init__(codigo, nombre)
        self._Precio = precio

    def getDescripcion(self):
        return self.getNombre() + self._Precio + "de color" + self.getColor()